package recommendation;

public class Compra {
	protected Comprador comprador;
	protected Produto produto;
	
	public Compra(Comprador comprador, Produto produto) {
		this.comprador = comprador;
		this.produto = produto;
	}
	
	public Comprador getComprador() {
		return comprador;
	}
	
	public Produto getProduto() {
		return produto;
	}
	
	public int getIdComprador() {
		return comprador.getId();
	}
	
	public int getIdProduto() {
		return produto.getId();
	}
}
