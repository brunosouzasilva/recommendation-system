package recommendation;

import gnu.prolog.database.PrologTextLoaderError;
import gnu.prolog.term.AtomTerm;
import gnu.prolog.term.CompoundTerm;
import gnu.prolog.term.Term;
import gnu.prolog.term.VariableTerm;
import gnu.prolog.vm.Environment;
import gnu.prolog.vm.Interpreter;
import gnu.prolog.vm.PrologCode;
import gnu.prolog.vm.PrologException;
import gnu.prolog.vm.Interpreter.Goal;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Base de recomendação baseada em prolog
 */
public class BaseRecomendacaoProlog extends BaseRecomendacao {
	protected String path;
	protected Environment env;
	protected Interpreter interpreter;
	protected List<Compra> cacheCompras;
	
	/**
	 * @param path Caminho do arquivo prolog com as cláusulas da base de recomendação
	 */
	public BaseRecomendacaoProlog(String path) {
		this.path = path;
		this.cacheCompras = new ArrayList<Compra>();
		
		env = new Environment();
		env.ensureLoaded(AtomTerm.get(path));
		interpreter = env.createInterpreter();
		env.runInitialization(interpreter);
		
		List<PrologTextLoaderError> loadErrors = env.getLoadingErrors();
		if (loadErrors.size() > 0) {
			System.err.println(loadErrors);
		}
	}
	
	public String getPath() {
		return path;
	}
	
	/**
	 * Adiciona uma compra à base de dados de recomendação
	 * @param comprador
	 * @param produto
	 */
	public void adicionarCompra(Compra compra) {
		cacheCompras.add(compra);
		try {
			FileWriter fw = new FileWriter(getPath(), true);
			fw.write("comprou(" + compra.getIdComprador() + "," + compra.getIdProduto() + ").\n");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Carrega as compras do arquivo para o cacheCompras
	 */
	protected void carregarCompras() {
		cacheCompras.clear();
		VariableTerm compradorTerm = new VariableTerm("Comprador");
		VariableTerm produtoTerm = new VariableTerm("Compra");
		Term[] args = { compradorTerm, produtoTerm };
		CompoundTerm comprouTerm = new CompoundTerm(AtomTerm.get("comprou"), args);		
		Goal goal = interpreter.prepareGoal(comprouTerm);
		
		try {
			while (true) {
				int res = interpreter.execute(goal);
				cacheCompras.add(new Compra(
					new Comprador(Integer.parseInt(compradorTerm.value.toString())), 
					new Produto(Integer.parseInt(produtoTerm.value.toString()))
				));
				
				if (res == PrologCode.SUCCESS_LAST)
					break;
			}
		} catch (PrologException e) {
			e.printStackTrace();
		}
	}
	
	public List<Compra> getCompras() {
		if (cacheCompras.isEmpty())
			carregarCompras();
		
		return cacheCompras;
	}
	
	/**
	 * @return Todos os compradores cadastrados no sistema de recomendação
	 */
	public List<Comprador> getCompradores() {
		List<Comprador> compradores = new ArrayList<Comprador>();
		
		for(Compra c : getCompras())
			compradores.add(c.getComprador());
		
		return compradores;
	}
	
	/**
	 * @param produto
	 * @return Todos os compradores do produto @param produto
	 */
	public List<Comprador> getCompradores(Produto produto) {
		List<Comprador> compradores = new ArrayList<Comprador>();
		
		for(Compra c : getCompras())
			if (c.getIdProduto() == produto.getId())
				compradores.add(c.getComprador());
		
		return compradores;
	}
	
	/**
	 * @return Todos os produtos cadastrados no sistema de recomendação
	 */
	public List<Produto> getProdutos() {
		List<Produto> produtos = new ArrayList<Produto>();

		for(Compra c : getCompras())
			produtos.add(c.getProduto());
		
		return produtos;
	}
	
	/**
	 * @param comprador
	 * @return Todos os produtos comprados pelo @param comprador
	 */
	public List<Produto> getProdutos(Comprador comprador) {
		List<Produto> produtos = new ArrayList<Produto>();
		
		for(Compra c : getCompras())
			if (c.getIdComprador() == comprador.getId())
				produtos.add(c.getProduto());
		
		return produtos;
	}
	
}
