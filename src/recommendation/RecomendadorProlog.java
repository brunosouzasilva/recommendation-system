package recommendation;

import gnu.prolog.database.PrologTextLoaderError;
import gnu.prolog.term.AtomTerm;
import gnu.prolog.term.CompoundTerm;
import gnu.prolog.term.IntegerTerm;
import gnu.prolog.term.Term;
import gnu.prolog.term.VariableTerm;
import gnu.prolog.vm.Environment;
import gnu.prolog.vm.Interpreter;
import gnu.prolog.vm.Interpreter.Goal;
import gnu.prolog.vm.PrologException;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Oferece serviços de recomendação. Atua sobre uma base de recomendação qualquer.
 * Os cálculos de recomendação são implementados em prolog e esta classe é responsável pelo mapeamento entre o Java e o Prolog
 */
public class RecomendadorProlog {
	protected Environment env;
	protected Interpreter interpreter;
	protected BaseRecomendacao base;
		
	/**
	 * @param base Base de recomendação sobre o qual o Recomendador irá atuar
	 */
	public RecomendadorProlog(BaseRecomendacao base) {
		this.base = base;
		setup();
	}
	
	private void setup() {
		env = new Environment();
		env.ensureLoaded(AtomTerm.get("src/recommendation/recommender.pro"));
		interpreter = env.createInterpreter();
		env.runInitialization(interpreter);
		
		List<PrologTextLoaderError> loadErrors = env.getLoadingErrors();
		if (loadErrors.size() > 0) {
			System.err.println(loadErrors);
		}
	}
	
	/**
	 * @param produto Produto a ser pesquisado
	 * @return Uma lista de produtos que são relacionados com o produto parâmetro
	 */
	public Collection<Produto> getProdutosRelacionados(Produto produto) {
		carregarBase();
		Collection<Produto> produtos = new HashSet<Produto>();
		
		IntegerTerm produtoTerm = new IntegerTerm(produto.id);
		VariableTerm outros_produtosTerm = new VariableTerm("Outros_Produtos");
		Term[] args = { produtoTerm, outros_produtosTerm };
		CompoundTerm comprou_tambemTerm = new CompoundTerm(AtomTerm.get("comprou_tambem"), args);		
		Goal goal = interpreter.prepareGoal(comprou_tambemTerm);
		
		try {
			interpreter.execute(goal);
			Collection<Term> col = new HashSet<Term>();
			CompoundTerm.toCollection(outros_produtosTerm, col);
			for(Term op : col) {
				String op_string = op.toString();
				int op_int = Integer.parseInt(op_string);
				produtos.add(new Produto(op_int));
			}
		} catch (PrologException e) {
			e.printStackTrace();
		}
		return produtos;
	}	

	/**
	 * @param comprador Comprador
	 * @return Uma coleção de produtos que são recomendados à comprador
	 */
	public Collection<Produto> getProdutosRecomendados(Comprador comprador) {
		Collection<Produto> produtos_recomendados = new HashSet<Produto>();
		List<Produto> meus_produtos = base.getProdutos(comprador);
		
		for(Produto meu_produto : meus_produtos)
			produtos_recomendados.addAll(getProdutosRelacionados(meu_produto));
		
		produtos_recomendados.removeAll(meus_produtos);
		
		return produtos_recomendados;
	}
	
	/**
	 * Carrega as cláusulas da Base de Recomendação para o recomendador 
	 */
	protected void carregarBase() {
		List<Compra> compras = base.getCompras();
		
		IntegerTerm compradorTerm;
		IntegerTerm produtoTerm;
		CompoundTerm comprouTerm;
		CompoundTerm assertz;
		
		for(Compra c : compras) {
			compradorTerm = new IntegerTerm(c.getIdComprador());
			produtoTerm = new IntegerTerm(c.getIdProduto());
			Term[] args = { compradorTerm, produtoTerm };
			comprouTerm = new CompoundTerm(AtomTerm.get("comprou"), args);
			Term[] args2 = { comprouTerm };
			assertz = new CompoundTerm(AtomTerm.get("assertz"), args2);
			try {
				interpreter.runOnce(assertz);
			} catch (PrologException e) {
				e.printStackTrace();
			}
		}
	}
}
