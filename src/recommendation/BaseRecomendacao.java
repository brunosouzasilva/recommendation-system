package recommendation;

import java.util.List;

/**
 * Uma base de recomendação é uma base de dados qualquer que persiste compradores e seus produtos
 */
public abstract class BaseRecomendacao {
	
	/**
	 * Adiciona uma compra à base de dados de recomendação
	 * @param comprador
	 * @param produto
	 */
	public abstract void adicionarCompra(Compra compra);
	
	/**
	 * @return Lista com todas as compras cadastradas na base de dados
	 */
	public abstract List<Compra> getCompras();
	
	/**
	 * @return Todos os compradores cadastrados no sistema de recomendação
	 */
	public abstract List<Comprador> getCompradores();
	
	/**
	 * @param produto
	 * @return Todos os compradores do produto @param produto
	 */
	public abstract List<Comprador> getCompradores(Produto produto);
	
	/**
	 * @return Todos os produtos cadastrados no sistema de recomendação
	 */
	public abstract List<Produto> getProdutos();
	
	/**
	 * @param comprador
	 * @return Todos os produtos comprados pelo @param comprador
	 */
	public abstract List<Produto> getProdutos(Comprador comprador);
}
