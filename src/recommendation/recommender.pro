/**
 * Recomendador. Responsável pela lógica de recomendação.
 * As cláusulas de persistência são mantidas em banco de dados separado e carregadas em tempo de execução.
 */

/**
 * Unifica em Compradores uma lista contendo todas as pessoas que compraram Produto
 */
compradores(Produto, Compradores) :- findall(Comprador, comprou(Comprador, Produto), Compradores).

/**
 * Unifica em Produtos uma lista contendo todos os produtos comprados por um Comprador
 * ou por uma lista de Compradores
 */
produtos([Comprador], Produtos) :- produtos(Comprador, Produtos), !.
produtos([Comprador|Outros_Compradores], Produtos) :- produtos(Comprador, P),
                                                      produtos(Outros_Compradores, Outros_P),
                                                      juntar_listas(P, Outros_P, Produtos_Duplicados),
                                                      remover_repetidos(Produtos_Duplicados, Produtos), !.
produtos(Comprador, Produtos) :- findall(Produto, comprou(Comprador, Produto), Produtos).

/**
 * Unifica em Produtos os outros produtos comprados pelas pessoas que compraram Produto
 */
comprou_tambem(Produto, Outros_Produtos) :- compradores(Produto, Compradores),
                                            produtos(Compradores, Produtos),
                                            remover(Produto, Produtos, Outros_Produtos).


%==================================================================%
%  Predicados auxiliares, e.g., manipuladores de listas em Prolog  %
%==================================================================%

/**
 * Concatena duas listas, primeiro parâmetro concatenado com o segundo
 */
juntar_listas([], Segunda_Lista, Segunda_Lista).
juntar_listas([H|T], Segunda_Lista, [H|Resultado]) :- juntar_listas(T, Segunda_Lista, Resultado).

/**
 * Verdadeiro quando a lista passada como parâmetro for vazia
 */
vazia([]).

/**
 * Verdadeiro quando Elemento pertence à lista
 */
pertence(_, []) :- fail.
pertence(Elemento, [Primeiro|Outros]) :- (Elemento = Primeiro; pertence(Elemento, Outros)), !.

/**
 * Remove a primeira aparição de Elemento na lista
 */
remover(_, [], []).
remover(Elemento, [H|T], Lista) :- Elemento = H, Lista = T, !.
remover(Elemento, [H|T], Lista) :- remover(Elemento, T, Sublista), Lista = [H|Sublista].

/**
 * Para cada elemento k que aparece duas ou mais vezes na lista, são removidas as suas cópias
 * até que reste apenas uma instância de k na lista
 */
remover_repetidos([], []).
remover_repetidos([H|T], Lista) :- remover_repetidos(T, Sublista),
                                   ((pertence(H, Sublista), Lista = Sublista); (Lista = [H|Sublista])), !.