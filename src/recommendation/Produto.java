package recommendation;

public class Produto {
	protected int id;
	
	public Produto(int id) {
		this.id = id;
	}
	
	/**
	 * @return id do produto
	 */
	public int getId() {
		return id;
	}
	
	@Override
	public int hashCode() {
		return (new Integer(id)).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Produto)) return false;
		Produto another = (Produto) obj;
		return another.id == this.id;
	}
	
}