package recommendation;

public class Comprador {
	protected int id;
	
	public Comprador(int id) {
		this.id = id;
	}
	
	/**
	 * @return id do comprador
	 */
	public int getId() {
		return id;
	}
	
}
