import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import recommendation.*;

public class Main {

	public static void main(String args[]) {

		BaseRecomendacao base = new BaseRecomendacaoProlog("src/db.pro");		
		RecomendadorProlog recomendador = new RecomendadorProlog(base);
		inicio(recomendador,base);
	}

	public static void inicio(RecomendadorProlog recomendador, BaseRecomendacao base) {
		
		Scanner scanner = new Scanner(System.in);
		int choice;
		boolean entradaValida = false;
		do {
			System.out.println("Menu : \n" + "       1 - Comprar produto\n"
					+ "       2 - Ver itens comprados\n"
					+ "       3 - Ver itens recomendados para você\n"
					+ "       4 - Ver itens relacionados\n"
					+ "       5 - Sair\n");

			choice = scanner.nextInt();
			if (choice >= 1 && choice <= 5) {
				entradaValida = true;
				if (entradaValida) {
					switch (choice) {
					case 1:
						System.out.println("Comprar Produto - Digite o seu id");
						int idUser = scanner.nextInt();
						System.out.println("Digite o id do produto");
						int idProduto = scanner.nextInt();
						Comprador comprador = new Comprador (idUser);
						Produto produto = new Produto(idProduto);
						Compra compra = new Compra (comprador, produto);
						base.adicionarCompra(compra);
						System.out.println("Compra Adicionada");
						break;
					case 2:
						System.out
								.println("Ver itens comprado - Digite o seu id");
						int idUser1 = scanner.nextInt();
						Comprador comprador1 = new Comprador(idUser1);
						List<Produto> produtosComprados = base.getProdutos(comprador1);
						System.out.println("Produtos Comprados : \n");
						for(Produto p :produtosComprados)
							System.out.println("                    "+" - Produto " + p.getId());
						System.out.println();
						break;
					case 3:
						System.out.println("Ver itens recomendados - Digite o seu id");
						int idUser3 = scanner.nextInt();
						Comprador comprador3 = new Comprador(idUser3);
						Collection<Produto> produtos_recomendados = recomendador.getProdutosRecomendados(comprador3);
						for(Produto p : produtos_recomendados)
							System.out.println("                    "+" - Produto " + p.getId());
						System.out.println();
						break;
					case 4:
						System.out
								.println("Ver itens relacionados - Digite o id do produto");
						int idProduto1 = scanner.nextInt();
						Produto produto1 = new Produto (idProduto1);
						Collection<Produto> produtos = recomendador.getProdutosRelacionados(produto1);
						System.out.println("Produtos Relacionados : \n");
						for(Produto p : produtos)
							System.out.println("                    "+" - Produto " + p.getId());
						System.out.println();
						break;
					default:
						System.out.println("Sair");

					}
					entradaValida = false;
				}
			} else {
				System.out.println("Escolha invalida");
			}
		} while (choice != 5);
		System.out.println("Fim");
		scanner.close();
	}

}
