## Integração de Paradigmas

Implementação de um sistema especialista utilizando a integração da linguagem de alto nível imperativa Java com a linguagem lógica Prolog.

Aplicação na qual a interface de usuário é implementada em Java e a base de conhecimento e regras de inferência são implementadas em Prolog.

Biblioteca utilizada para interação entre Java e Prolog: GNU Prolog for Java 0.2.6

## Autores

* Bruno Souza da Silva - 12100737
* Carlos Bonetti       - 12100739
* Guilherme Trilha     - 12100749
